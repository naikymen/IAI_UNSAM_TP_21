- [x] Reg. logistica
  - [ ] Agregar curva PR.

- [x] SVM
  - [ ] Hacerlo multiclase (skipped)
  - [ ] Agregar curva PR.

- [x] Algo con ensemble methods.
  - [x] Boosting.
  - [ ] Bagging.

- [ ] ANNs / CNNs  (skipped)
  - [ ] ¿GA para hacer redes chiquitas?
